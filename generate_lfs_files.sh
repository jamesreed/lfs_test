#!/bin/bash

for ((i=0; i<10; i++)); do
    filename=$(printf "%05d.iamlfs" $i)
    dd if=/dev/urandom of="lfs-objects-here/$filename" bs=1M count=1 status=none
    echo "Created $filename"
done
